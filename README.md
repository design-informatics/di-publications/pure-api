# PURE Api

Basic python script to pull DI publications from the PURE API and parse the XML response into a bibtext file.

## Running the script
Note: you will need to include the API_KEY as an environment variable. DO NOT COMMIT YOUR KEY TO THIS REPO!

Once you have the API_KEY in your environment, run `python3 process.py` to generate the bibtex file.

## Updating the DI Website
1. Upload the bibtext file via the wordpress admin interface on the DI website (*Media > Add New*). Note: you will need to change the file extension from .bibtex to .txt to allow the upload.

2. Click on the uploaded file and copy the file URL.

3. In the admin interface go to *Appearence > Theme file editor*, then navigate to the file `views/partials/head.blade.php` and update the bibtex src URL on line 63 to the url copied previously, adding `?v=2` to the end of the URL. For example:

`<bibtex src="https://www.designinformatics.org/wp-content/uploads/2022/10/publications.txt?v=2"></bibtex>`
