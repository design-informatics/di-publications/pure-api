import requests
import xml.etree.ElementTree as ET
from html2text import html2text
import os
from dotenv import load_dotenv

load_dotenv()

url = "https://www.pure.ed.ac.uk/ws/api/524/organisational-units/035197e2-377f-4837-97f7-7e235a2bbb93/research-outputs?rendering=BIBTEX&size=10000&offset=0&apiKey=" + os.getenv('API_KEY')

bib = ""
response = requests.request("GET", url, headers={}, data={})
print(response.text)
root = ET.fromstring(response.text)

for child in root.iter('rendering'):
    bib += html2text(child.text).replace('\n',' ') + '\n\n'

f = open("publications.bibtex", "w")
f.write(bib)
f.close()